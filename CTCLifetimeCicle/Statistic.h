#pragma once
#include <vector>

class IObject;

struct Statistic
{
	std::vector<class IObject*> LivingObjects;

	int LivingCubeNum{ 0 };
	int DeathCubeNum{ 0 };
	int TotalCubeLifeTime{ 0 };
	float AverageCubeLifeTime{ 0 };
	int MaxCubeLifeTime{ 0 };
	int TotalSpawnedCube{ 0 };

	int LivingCircleNum{ 0 };
	int DeathCircleNum{ 0 };
	int TotalCircleLifeTime{ 0 };
	float AverageCircleLifeTime{ 0 };
	int MaxCircleLifeTime{ 0 };
	int TotalSpawnedCircle{ 0 };

	int LivingTriangleNum{ 0 };
	int DeathTriangleNum{ 0 };
	int TotalTriangleLifeTime{ 0 };
	int MaxTriangleLifeTime{ 0 };
	int TotalSpawnedTriangle{ 0 };

	int Totaldays{ 0 };
};

class StatisticHandler
{
public:
	StatisticHandler();
	StatisticHandler(Statistic& NewStatCollecor);
	~StatisticHandler();

private:
	float AverageLifeTime{ 0 };
	Statistic* StatisticColector;
	float CalculateAverageLifeTime();
public:
	void ShowStatistic();
	void ShowMaxLifetime();
};
