#include "Triangle.h"
#include <iostream>

Triangle::Triangle()
{
	this->SetName();
	std::cout << "Triangle spawned\n";

}

Triangle::~Triangle()
{
	std::cout << "!!!Noooo. My Triangle life is over\n";
}

void Triangle::SetName()
{
	this->Name = ObjectsName::TRIANGLE;
}

int Triangle::BigStrongFunction()
{
	if (GetLifeTime() == 3)
	{
		std::cout << "ThReaper lives 3 day on Pandora!\n";
	}
	return 0;
}

