#pragma once
#include <random>
#include "Statistic.h"
#include "ObjectInterface.h"

class Destructor
{
public:
	Destructor();
	Destructor(int MinCount, int MaxCount, Statistic& NewStatCollecor);
	~Destructor();
private:
	int MaxDeathCount;
	int MinDeathCount;
	std::mt19937 RandomGen;
	Statistic* StatisticColector;
	int CurrentCubeCount{ 0 };
	int CurrentCircleCount{ 0 };
	int CurrentTriangeCount{ 0 };
public:
	void SetMinMaxSpawnCount(int MinCount, int MaxCount);
	void Genocide();
private:
	int GetLifetimeOfObject(IObject* Object);
	void StatisticChronicer(IObject* Object);
	void Death(IObject* Object);
	void Death(std::vector<IObject*>::iterator& Iterator);
};

