#include "Destructor.h"
#include <iostream>
#include <ctime>
#include "Cube.h"
#include "Triangle.h"
#include "Circle.h"

Destructor::Destructor()
{
	RandomGen.seed(std::time(nullptr));
}

Destructor::Destructor(int MinCount, int MaxCount, Statistic& NewStatCollecor)
{
	RandomGen.seed(std::time(nullptr));
	MinDeathCount = MinCount;
	MaxDeathCount = MaxCount;
	StatisticColector = &NewStatCollecor;

}

Destructor::~Destructor()
{
}

void Destructor::SetMinMaxSpawnCount(int MinCount, int MaxCount)
{
	MinDeathCount = MinCount;
	MaxDeathCount = MaxCount;
}

void Destructor::Genocide()
{
	int AllToDeath = MinDeathCount + RandomGen() % (MaxDeathCount - MinDeathCount);
	if (AllToDeath > StatisticColector->LivingObjects.size())
		AllToDeath = StatisticColector->LivingObjects.size();
	auto Iterator = StatisticColector->LivingObjects.begin();
	for (int i = 0; i < AllToDeath; i++)
	{
		Iterator += RandomGen() % StatisticColector->LivingObjects.size();
		if (!!*Iterator)
		{
			Death(Iterator);
			StatisticColector->LivingObjects.erase(Iterator);
			Iterator = StatisticColector->LivingObjects.begin();
		}
	}
}

int Destructor::GetLifetimeOfObject(IObject* Object)
{
	return Object->GetLifeTime();
}

void Destructor::StatisticChronicer(IObject* Object)
{
	switch (Object->GetName())
	{
	case ObjectsName::TRIANGLE:
		StatisticColector->TotalTriangleLifeTime += GetLifetimeOfObject(Object);
		StatisticColector->DeathTriangleNum++;
		--StatisticColector->LivingTriangleNum;
		if (GetLifetimeOfObject(Object) > StatisticColector->MaxTriangleLifeTime)
		{
			StatisticColector->MaxTriangleLifeTime = GetLifetimeOfObject(Object);
		}
		break;
	case ObjectsName::CIRCLE:
		StatisticColector->TotalCircleLifeTime += GetLifetimeOfObject(Object);
		StatisticColector->DeathCircleNum++;
		--StatisticColector->LivingCircleNum;
		if (GetLifetimeOfObject(Object) > StatisticColector->MaxCircleLifeTime)
		{
			StatisticColector->MaxCircleLifeTime = GetLifetimeOfObject(Object);
		}
		break;
	case ObjectsName::CUBE:
		StatisticColector->TotalCubeLifeTime += GetLifetimeOfObject(Object);
		StatisticColector->DeathCubeNum++;
		--StatisticColector->LivingCubeNum;
		if (GetLifetimeOfObject(Object) > StatisticColector->MaxCubeLifeTime)
		{
			StatisticColector->MaxCubeLifeTime = GetLifetimeOfObject(Object);
		}
		break;
	}
}

void Destructor::Death(IObject* Object)
{
	StatisticChronicer(Object);
	//std::cout << "Dead object - " << Object->GetName() << '\n';
	delete Object;
}

void Destructor::Death(std::vector<IObject*>::iterator& Iterator)
{
	StatisticChronicer(*Iterator);
	//std::cout << "Dead object - " << (*Iterator)->GetName() << '\n';
	delete (*Iterator);
}
