#pragma once
#include "ObjectInterface.h"


class Cube : public IObject
{
public:
	Cube();
	~Cube()override;
private:

public:
	void SetName() override;
	int BigStrongFunction() override;

};

