#pragma once
#include "ObjectInterface.h"
class Circle : public IObject
{
public:
	Circle();
	~Circle() override;
private:

public:
	void SetName() override;
	int BigStrongFunction() override;

};

