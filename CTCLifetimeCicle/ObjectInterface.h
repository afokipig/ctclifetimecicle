#pragma once
#include <string>
#include "Statistic.h"

enum ObjectsName
{
	TRIANGLE,
	CIRCLE,
	CUBE
};

class IObject
{
public:
	IObject();
	virtual ~IObject();
private:
	int LifeTime;
protected:
	ObjectsName Name;
public:
public:
	virtual int GetLifeTime();
	virtual void SetLifeTme(int NewTime);
	virtual ObjectsName GetName();
	virtual void SetName() = 0;
	virtual void TickAddLifeTime();
	virtual int BigStrongFunction() = 0;
};
