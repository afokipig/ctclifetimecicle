#include "Spawner.h"
#include <iostream>
#include <ctime>
#include "Cube.h"
#include "Triangle.h"
#include "Circle.h"

Spawner::Spawner()
{
	RandomGen.seed(std::time(nullptr));
}

Spawner::Spawner(int MinCount, int MaxCount, Statistic &NewStatCollecor)
{
	RandomGen.seed(std::time(nullptr));
	MinSpawnCount = MinCount;
	MaxSpawnCount = MaxCount;
	StatisticColector = &NewStatCollecor;
}

Spawner::~Spawner()
{
}

void Spawner::Spawn()
{
	int AllToSpawn = MinSpawnCount + RandomGen() % (MaxSpawnCount - MinSpawnCount);
	std::vector<int> Temp{ 0,0,0 };

	IObject* MyObject;
	for (int i = 0; i < AllToSpawn; i++)
	{
		switch (RandomGen() % 3)
		{
		case 0:
			MyObject = new Triangle();
			StatisticColector->LivingObjects.push_back(MyObject);
			++StatisticColector->TotalSpawnedTriangle;
			++StatisticColector->LivingTriangleNum;
			break;
		case 1:
			MyObject = new Circle();
			StatisticColector->LivingObjects.push_back(MyObject);
			++StatisticColector->TotalSpawnedCircle;
			++StatisticColector->LivingCircleNum;
			break;
		case 2:
			MyObject = new Cube();
			StatisticColector->LivingObjects.push_back(MyObject);
			++StatisticColector->TotalSpawnedCube;
			++StatisticColector->LivingCubeNum;
			break;
		}
	}
}

void Spawner::SetMinMaxSpawnCount(int MinCount, int MaxCount)
{
	MaxSpawnCount = MaxCount;
	MinSpawnCount = MinCount;
}
