#pragma once
#include "ObjectInterface.h"
class Triangle : public IObject
{
public:
	Triangle();
	~Triangle() override;
private:

public:
	void SetName() override;
	int BigStrongFunction() override;
};

