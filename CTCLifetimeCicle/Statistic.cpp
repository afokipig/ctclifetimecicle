#include <iostream>
#include "Statistic.h"

StatisticHandler::StatisticHandler()
{
}

StatisticHandler::StatisticHandler(Statistic& NewStatCollecor)
{
	StatisticColector = &NewStatCollecor;
}

StatisticHandler::~StatisticHandler()
{
}

float StatisticHandler::CalculateAverageLifeTime()
{
	float TotalLifeTime = StatisticColector->TotalCircleLifeTime +
		StatisticColector->TotalCubeLifeTime +
		StatisticColector->TotalTriangleLifeTime;
	float TotalObjects = StatisticColector->DeathCircleNum +
		StatisticColector->DeathCubeNum +
		StatisticColector->DeathTriangleNum;
	AverageLifeTime = TotalLifeTime / TotalObjects;
		return AverageLifeTime;
}

void StatisticHandler::ShowStatistic()
{
	std::cout << "Today " << StatisticColector->Totaldays << " day.\n"
		<< "On this moment number of cubes - " << StatisticColector->LivingCubeNum << ".\n"
		<< "Number of circles - " << StatisticColector->LivingCircleNum << ".\n"
		<< "Number of triangles - " << StatisticColector->LivingTriangleNum << ".\n"
		<< "Average life expectancy - " << CalculateAverageLifeTime() << ".\n";
		
}

void StatisticHandler::ShowMaxLifetime()
{
	std::cout << "Max lifitime of circles - " << StatisticColector->MaxCircleLifeTime << ".\n"
		<< "Max lifetime of cubes - " << StatisticColector->MaxCubeLifeTime << ".\n"
		<< "Max lifetime of triangles - " << StatisticColector->MaxTriangleLifeTime << ".\n";
}
