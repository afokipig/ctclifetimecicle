﻿// CTCLifetimeCicle.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <Windows.h>
#include "Statistic.h"
#include "ObjectInterface.h"
#include "Cube.h"
#include "Triangle.h"
#include "Circle.h"
#include "Spawner.h"
#include "Destructor.h"


int main()
{
	Statistic MyStats;
	Spawner MySpawner(5, 9, MyStats);
	Destructor MyDestructor(2, 12, MyStats);
	StatisticHandler MyStatsHandler(MyStats);
	for (int i = 0; i < 100; i++)
	{
		MyStats.Totaldays++;
		MySpawner.Spawn();
		for (auto Object : MyStats.LivingObjects)
		{
			Object->TickAddLifeTime();
			Object->BigStrongFunction();
		}
		MyDestructor.Genocide();
		std::cout << "\n*******************************************\n";
		MyStatsHandler.ShowStatistic();
		std::cout << "*******************************************\n\n";
		Sleep(1000);
	}
	MyStatsHandler.ShowMaxLifetime();
	int a;
	std::cin >> a;
}
