#pragma once
#include <random>
#include "Statistic.h"

class Spawner
{
public:
	Spawner();
	Spawner(int MinCount, int MaxCount, Statistic &NewStatCollecor);
	~Spawner();
private:
	int MaxSpawnCount;
	int MinSpawnCount;
	std::mt19937 RandomGen;
	Statistic *StatisticColector;
	
public:
	void Spawn();
	void SetMinMaxSpawnCount(int MinCount, int MaxCount);
};

